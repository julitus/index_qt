#ifndef CREATOR_H
#define CREATOR_H

#include "textbox.h"
#include "button.h"
#include "gridtable.h"
#include <QWidget>
//#include "utility.h"
#include "table.h"
#include "combobox.h"

class Creator : public QWidget{

    Q_OBJECT

    public:
        Creator(QWidget * parent = 0);
        virtual ~Creator();

    private slots:
        bool add();
        void find();
        bool clean();
        void start();
        void prev();
        void next();
        void end();
        void bring();
        void cellSelected();


    private:
        void printRegister(int i, std::string str);
        bool findRegister(QString s);
        void stateButtonMove(bool state);
        ComboBox *semester;
        TextBox *code;
        TextBox *code_find;
        TextBox *name;
        TextBox *credit;
        TextBox *h_teo;
        TextBox *h_pra;
        TextBox *h_lab;
        Button *btn_add;
        Button *btn_find;
        Button *btn_clean;
        Button *btn_start;
        Button *btn_end;
        Button *btn_next;
        Button *btn_prev;
        GridTable *grid;
        Table *courses;

};

#endif // CREATOR_H


