#ifndef COMBOBOX_H
#define COMBOBOX_H
#include <QComboBox>

class ComboBox: public QComboBox{

    public:
        ComboBox(QPoint point, QSize size, QWidget * parent = 0);
        ~ComboBox();

};

#endif // COMBOBOX_H
