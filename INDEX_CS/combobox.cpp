#include "combobox.h"

ComboBox::ComboBox(QPoint point, QSize size, QWidget *parent): QComboBox(parent){

    this->setGeometry(QRect(point,  size));
    QStringList list=(QStringList()<<"-- Seleccione semestre --"<<
                      "I SEMESTRE"<<"II SEMESTRE"<<"III SEMESTRE"<<
                      "IV SEMESTRE"<<"V SEMESTRE"<<"VI SEMESTRE"<<
                      "VII SEMESTRE"<<"VIII SEMESTRE"<<"IX SEMESTRE"<<"X SEMESTRE");
    this->addItems(list);

}

ComboBox::~ComboBox(){

}
