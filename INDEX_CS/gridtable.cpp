#include "gridtable.h"
#include <QHeaderView>
#include <QApplication>
#include <QDesktopWidget>

GridTable::GridTable(QPoint point, QSize size, QWidget *parent): QTableWidget(parent){

    this->setColumnCount(6);
    QStringList header;
    header<<"Codigo"<<"Nombre"<<"Creditos"<<"H. Teoria"<<"H. Pract."<<"H. Lab.";
    this->setHorizontalHeaderLabels(header);
    this->verticalHeader()->setVisible(false);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->setSelectionMode(QAbstractItemView::SingleSelection);
    this->setShowGrid(false);
    this->setStyleSheet("QTableView {selection-background-color: #5a76d0;}");
    this->setGeometry(QRect(point, size));
    this->setColumnWidth(0, 60);
    this->setColumnWidth(1, 250);
    this->setColumnWidth(2, 60);
    this->setColumnWidth(3, 60);
    this->setColumnWidth(4, 60);
    this->setColumnWidth(5, 60);

}

GridTable::~GridTable()
{

}

