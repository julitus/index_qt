#include "button.h"

Button::Button(std::string str, QPoint point, QSize size, QWidget *parent): QPushButton(QString::fromStdString(str), parent){

    this->setGeometry(QRect(point, size));

}

Button::~Button(){

}

