#include "creator.h"
#include "label.h"
#include "ntypes.h"
#include <sstream>
#include <QHBoxLayout>
//#include "utility.h"

Creator::Creator(QWidget * parent): QWidget(parent){

    this->courses = new Table(FILE_COURSE, FILE_PRE_COURSES);
    this->courses->index();

    //Damos un alto y ancho minimo
    this->setMinimumSize(WIDTH_WINDOW, HEIGHT_WINDOW);

    Label *title = new Label("CS UNSA", LBL_UNSA, this);
    Label *l1 = new Label("CODIGO", LBL_CODE, this);
    Label *l2 = new Label("NOMBRE", LBL_NAME, this);
    Label *l3 = new Label("CREDITOS", LBL_CRED, this);
    Label *l4 = new Label("H. TEO.", LBL_H_TEO, this);
    Label *l5 = new Label("H. PRA.", LBL_H_PRA, this);
    Label *l6 = new Label("H. LAB.", LBL_H_LAB, this);

    this->code = new TextBox(TXB_CODE, this);
    this->name = new TextBox(TXB_NAME, this);
    this->credit = new TextBox(TXB_CRED, this);
    this->h_teo = new TextBox(TXB_H_TEO, this);
    this->h_pra = new TextBox(TXB_H_PRA, this);
    this->h_lab = new TextBox(TXB_H_LAB, this);

    //creamos las acciones y las conectamos con un metodo de la clase
    this->btn_add = new Button("AGREGAR", BTN_ADD, BTN_SIZE_GENERAL, this);
    connect(this->btn_add, SIGNAL (released()), this, SLOT (add()));
    this->btn_clean = new Button("LIMPIAR", BTN_CLEAN, BTN_SIZE_GENERAL, this);
    connect(this->btn_clean, SIGNAL (released()), this, SLOT (clean()));

    this->code_find = new TextBox(TXB_CODE_FIND, this);
    this->code_find->setPlaceholderText("codigo");
    connect(this->code_find, SIGNAL(returnPressed()), this, SLOT(find()));

    this->btn_find = new Button("BUSCAR", BTN_FIND, BTN_SIZE_GENERAL, this);
    connect(this->btn_find, SIGNAL (released()), this, SLOT (find()));

    this->semester = new ComboBox(CBX_SEMESTER,  CBX_SEMESTER_SIZE, this);
    connect(this->semester, SIGNAL(currentIndexChanged(int)), this, SLOT(bring()));

    this->btn_start = new Button("<<", BTN_START, BTN_SIZE_MOVES, this);
    connect(this->btn_start, SIGNAL (released()), this, SLOT (start()));
    this->btn_prev = new Button("<", BTN_PREV, BTN_SIZE_MOVES, this);
    connect(this->btn_prev, SIGNAL (released()), this, SLOT (prev()));
    this->btn_next = new Button(">", BTN_NEXT, BTN_SIZE_MOVES, this);
    connect(this->btn_next, SIGNAL (released()), this, SLOT (next()));
    this->btn_end = new Button(">>", BTN_END, BTN_SIZE_MOVES, this);
    connect(this->btn_end, SIGNAL (released()), this, SLOT (end()));

    this->grid = new GridTable(GRD_COURSE_POS, GRD_COURSE, this);
    connect(this->grid, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(cellSelected()));

    this->stateButtonMove(false);

}

bool Creator::add(){
    //creamos la ventana de advertencia
    QMessageBox::StandardButton msg;
    msg = QMessageBox::question(this, "Advertencia", "Agregara este registro ¿esta Ud. seguro?",
                                QMessageBox::Yes|QMessageBox::No);
    //comprobamos si fue aceptada la peticion
    if(msg == QMessageBox::Yes) {
        //limpiamos

        if(!courses->findRecord(qstring_to_char(code->text()), false))
        {
            QString data = code->text()+","+name->text()+","+credit->text()+","+h_teo->text()+","+h_pra->text()+","+h_lab->text();

            courses->insertRecord(qstring_to_char(data));
        }
        return true;
    }
    return false;


}

void Creator::find(){

    if(!this->findRegister(this->code_find->text())){
        QMessageBox::information(this, "Informacion", "No existe el codigo!!");
    }

}

bool Creator::clean(){

    //creamos la ventana de advertencia
    QMessageBox::StandardButton msg;
    msg = QMessageBox::question(this, "Advertencia", "Limpiara las cajas de texto ¿esta Ud. seguro?",
                                QMessageBox::Yes|QMessageBox::No);
    //comprobamos si fue aceptada la peticion
    if(msg == QMessageBox::Yes) {
        //limpiamos
        this->code->setText("");
        this->name->setText("");
        this->credit->setText("");
        this->h_teo->setText("");
        this->h_pra->setText("");
        this->h_lab->setText("");
        return true;
    }
    return false;

}

void Creator::start(){

    courses->start();
    grid->setRowCount(1);
    printRegister(0, courses->getRecord());

}

void Creator::prev(){

    courses->previous();
    grid->setRowCount(1);
    printRegister(0, courses->getRecord());

}

void Creator::next(){

    courses->next();
    grid->setRowCount(1);
    printRegister(0, courses->getRecord());

}

void Creator::end(){

    courses->end();
    grid->setRowCount(1);
    printRegister(0, courses->getRecord());

}

void Creator::bring(){

    int val = semester->currentIndex();
    if(val){
      /*  int n = (val+1) / 2;
        char *ss = new char[3];
        ss[0] = n + '0';
        ss[1] = (val % 2) ? 1 + '0': 2 + '0';
        ss[2] = '\0';*/

        vector<std::string> v = courses->getRecords(get_cod_semester(val));
        grid->setRowCount(v.size());
        for(int unsigned i = 0; i < v.size(); ++i){
            printRegister(i, v[i]);
        }
        stateButtonMove(false);
        this->code_find->setText("");
    }else{
        grid->setRowCount(0);
        stateButtonMove(true);
    }

}

void Creator::printRegister(int i, std::string str){
     vector<QString> row=separate_data(str);
    /*std::stringstream ss(str);
    vector<QString> row;
    string c_str;
    qDebug() << QString::fromStdString(str);
    while(getline(ss, c_str, ',')){
        row.push_back(QString::fromStdString(c_str));
        qDebug() << QString::fromStdString(c_str);*/

    grid->setItem(i, 0, new QTableWidgetItem(row[0]));
    grid->setItem(i, 1, new QTableWidgetItem(row[1]));
    grid->setItem(i, 2, new QTableWidgetItem(row[2]));
    grid->setItem(i, 3, new QTableWidgetItem(row[3]));
    grid->setItem(i, 4, new QTableWidgetItem(row[4]));
    grid->setItem(i, 5, new QTableWidgetItem(row[5]));
}

bool Creator::findRegister(QString s){

    /*std::string str = s.toStdString();
    char *c = new char[str.size() + 1];
    std::copy(str.begin(), str.end(), c);
    c[str.size()] = '\0'*/


    if(courses->findRecord(qstring_to_char(s), true)){
        grid->setRowCount(1);
        printRegister(0, courses->getRecord());
        stateButtonMove(true);
        return true;
    }
    grid->setRowCount(0);
    stateButtonMove(false);
    return false;

}

void Creator::stateButtonMove(bool state){

    btn_start->setEnabled(state);
    btn_prev->setEnabled(state);
    btn_next->setEnabled(state);
    btn_end->setEnabled(state);

}

void Creator::cellSelected(){

    QString str;
    QString lbl[] = {"Cod.: ", "Cur.: ", "Cred: ", "H. T: ", "H. P: ", "H. L: "};
    int i = 0;
    foreach(QTableWidgetItem *t, grid->selectedItems()){
        str += lbl[i]+ t->text() + '\n';
        i++;
    }
   /* std::string s = grid->selectedItems().at(0)->text().toStdString();
    char *c = new char[s.size() + 1];
    std::copy(s.begin(), s.end(), c);
    c[s.size()] = '\0';*/


    str += "\nPrerequisitos:\n";

    vector<std::string> v = courses->getPres(qstring_to_char(grid->selectedItems().at(0)->text()));
    for(int unsigned i = 0; i < v.size(); ++i){
      /*  std::stringstream ss(v[i]);
        vector<QString> row;
        string c_str;
        while(getline(ss, c_str, ',')){
            row.push_back(QString::fromStdString(c_str));
        }*/
                vector<QString> row= separate_data(v[i]);

        str += QString::number(i+1) + ": " + row[0] + " - " + row[1] + "\n";
        row.clear();
    }
    QMessageBox::information(this, "Informacion", str);

}

Creator::~Creator(){

}
