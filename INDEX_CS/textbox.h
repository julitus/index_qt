#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <QLineEdit>

class TextBox : public QLineEdit{

    public:
        TextBox(QPoint point, QWidget * parent = 0);
        ~TextBox();
};

#endif // TEXTBOX_H
