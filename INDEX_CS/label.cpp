#include "label.h"

Label::Label(std::string str, QPoint point, QWidget *parent) : QLabel(parent){

    this->setStyleSheet("QLabel { background-color: #f9f6e8;"
                        "         border-style: outset;"
                        "         border-width: 2px;"
                        "         border-radius: 5px;"
                        "         border-color: #f9f6e8;"
                        "         color: #08036a;"
                        "         font: bold 14px; }");
    this->setText(QString::fromStdString(str));
    this->move(point);

}

Label::~Label()
{

}

