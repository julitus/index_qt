#ifndef TABLE_H
#define TABLE_H
#include <list>
#include <vector>
#include <iostream>
//#include "utility.h"
using namespace std;

struct Record{

    char key[10];
    long start;
    long end;

};

struct Pre{

    char key[10];
    list<string> pres;

};

class Table{

    private:
        list<Record> courses;
        list<Pre> pre_courses;
        char *name_file;
        char *name_pre;
        list<Record>::iterator act;
        string getRecord(list<Record>::iterator it);
        list<Pre>::iterator findPre(char *k);
        list<Record>::iterator findIt(char *key);
        /*list<Record>::iterator find(char *key);*/
    public:
        Table(char *file, char *file_pre);
        void index();
        void start();
        void end();
        string getRecord();
        void  next();
        void previous();
        bool findRecord(char *key, bool change);
        void insertRecord(char *data);
        vector<string> getRecords(char *s);
        vector<string> getPres(char *s);
        ~Table();

};

#endif // TABLE_H
