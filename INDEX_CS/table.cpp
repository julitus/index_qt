#include "table.h"
#include <QDebug>
#include <fstream>
#include <sstream>
//#include "utility.h"

Table::Table(char *file, char *file_pre){

    name_file = file;
    name_pre = file_pre;

}

void Table::index(){

    char key[10];
    char buffer[1000];
    long offset = 0;
    list<Pre>::iterator it;

    ifstream in(name_pre);

    if(in.good()){
        qDebug() << "OK1!!!!";
        while(!in.eof()){
            in.getline(key, 11, ',');
            in.getline(buffer, 1000);
            if(strlen(key)){
                it = findPre(key);
                if(it == pre_courses.end()){
                    Pre P;
                    strcpy(P.key , key);
                    /*strcpy(key , buffer);*/
                    P.pres.push_back(buffer);
                    pre_courses.push_back(P);
                }else{
                    /*strcpy(key , buffer);*/
                    it->pres.push_back(buffer);
                }
            }
        }
        in.close();
    }

    in.open(name_file);

    if(in.good()){
        qDebug() << "OK2!!!!";
        while(!in.eof()){
            /*qDebug() << in.tellg();*/
            in.getline(key, 11, ',');
            in.getline(buffer, 1000);
            if(strlen(key)){
                Record R;
                strcpy(R.key , key);
                R.start = offset;
                offset += strlen(key) + strlen(buffer) + 3;
                R.end = offset - 1;
                courses.push_back(R);
                //qDebug() << strlen(buffer);
               // qDebug() << offset;*/
            }
        }
        in.close();
    }

}

void Table::start(){
    act = courses.begin();
}

void Table::end(){
    act = --(courses.end());
}

string Table::getRecord(list<Record>::iterator it){

    char data[1000];
    if(it != courses.end()){
        ifstream file(name_file);
        file.seekg((*it).start, ios::beg);
        file.getline(data, 1000);
        qDebug() << data;
        return string(data);
    }
    return "";

}

list<Pre>::iterator Table::findPre(char *k){

    list<Pre>::iterator it = pre_courses.begin();
    for(; it != pre_courses.end(); ++it){
        if(!strcmp((*it).key, k)){
            return it;
        }
    }
    return it;

}

list<Record>::iterator Table::findIt(char *k){

    list<Record>::iterator it = courses.begin();
    for(; it != courses.end(); it++){
        if(!strcmp((*it).key, k)){
            return it;
        }
    }
    return it;

}

string Table::getRecord(){

    return getRecord(act);

}

void Table::next(){
    if(act != --(courses.end())){
        act++;
    }
}

void Table::previous(){
    if(act != courses.begin()){
        act--;
    }
}

bool Table::findRecord(char *k, bool change){

    list<Record>::iterator it = findIt(k);
    if(it != courses.end()){
        if(change)
            act = it;
        return true;
    }
    return false;

}

void Table::insertRecord(char *data){

    ofstream ofs(name_file, ofstream::out | ofstream::app);
    ofs << data << "\n";
    ofs.close();
    std::stringstream ss;
    ss << data;
    char key[10];
    long siz = (--courses.end())->end + 1;
    ss.getline(key, 11, ',');
    Record R;
    strcpy(R.key , key);
    R.start = siz;
    R.end = siz + strlen(data) + 1;
    courses.push_back(R);
}

vector<string> Table::getRecords(char *s){

    vector<list<Record>::iterator> sem;
    vector<string> data;
    for(list<Record>::iterator it = courses.begin(); it != courses.end(); it++){
        if((*it).key[3] == s[0] and (*it).key[4] == s[1] ){
            sem.push_back(it);
        }
    }
    for(int unsigned i = 0; i < sem.size(); ++i){
        data.push_back(getRecord(sem[i]));
    }
    return data;

}

vector<string> Table::getPres(char *s){

    list<Pre>::iterator ut = findPre(s);
    vector<string> data;
    if(ut != pre_courses.end()){
        for(list<string>::iterator it = ut->pres.begin(); it != ut->pres.end(); ++it){
            /*qDebug() << QString::fromStdString(*it);*/
            char *c = new char[(*it).size() + 1];
            std::copy((*it).begin(), (*it).end(), c);
            c[(*it).size()] = '\0';

            data.push_back(getRecord(findIt(c)));
        }
    }
    return data;

}

Table::~Table(){

}
