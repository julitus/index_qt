#ifndef BUTTON_H
#define BUTTON_H
#include <QPushButton>

class Button: public QPushButton{

    public:
        Button(std::string str, QPoint point, QSize size, QWidget * parent = 0);
        ~Button();

};

#endif // BUTTON_H
