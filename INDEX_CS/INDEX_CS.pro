#-------------------------------------------------
#
# Project created by QtCreator 2015-09-12T09:16:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = INDEX_CS
TEMPLATE = app


SOURCES += main.cpp\
    creator.cpp \
    label.cpp \
    textbox.cpp \
    button.cpp \
    gridtable.cpp \
    table.cpp \
    combobox.cpp

HEADERS  += \
    creator.h \
    label.h \
    ntypes.h \
    textbox.h \
    button.h \
    gridtable.h \
    table.h \
    combobox.h \
    utility.h

DISTFILES +=

RESOURCES += \
    resources.qrc
