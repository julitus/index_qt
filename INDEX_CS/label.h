#ifndef LABEL_H
#define LABEL_H

#include <QLabel>

class Label : public QLabel{

    public:
        Label(std::string str, QPoint point, QWidget * parent = 0);
        ~Label();
};

#endif // LABEL_H
