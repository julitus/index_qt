#ifndef UTILITY_H
#define UTILITY_H
#include <vector>
#include <QString>
#include <sstream>

using namespace std;

char * get_cod_semester(int val)
{
    int n = (val+1) / 2;
    char *ss = new char[3];
    ss[0] = n + '0';
    ss[1] = (val % 2) ? 1 + '0': 2 + '0';
    ss[2] = '\0';
    return ss;
}

vector<QString> separate_data(string str ){
    std::stringstream ss(str);
    vector<QString> row;
    string c_str;
    /*qDebug() << QString::fromStdString(str);*/
    while(getline(ss, c_str, ',')){
        row.push_back(QString::fromStdString(c_str));
        /*qDebug() << QString::fromStdString(c_str);*/
    }
    return row;
}

char * qstring_to_char(QString s)
{
    std::string str = s.toStdString();
    char *c = new char[str.size() + 1];
    std::copy(str.begin(), str.end(), c);
    c[str.size()] = '\0';
    return c;
}

char * string_to_char(string str)
{
    //std::string str = s.toStdString();
    char *c = new char[str.size() + 1];
    std::copy(str.begin(), str.end(), c);
    c[str.size()] = '\0';
    return c;
}





#endif // UTILITY_H


