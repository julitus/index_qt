#ifndef NTYPES
#define NTYPES
#include <QtWidgets>
#include <QPoint>
#include <QSize>
#include "utility.h"

/*constantes*/
const unsigned int WIDTH_WINDOW = 600;
const unsigned int HEIGHT_WINDOW = 570;

const int VAR_X = 10;
const int VAR_Y = 10;

char *FILE_COURSE = "../INDEX_CS/files/cursos.dat";
char *FILE_PRE_COURSES = "../INDEX_CS/files/pre.dat";
/*char *FILE_COURSE = "cursos.dat";
char *FILE_PRE_COURSES = "pre.dat";*/

QPoint LBL_UNSA(240+VAR_X, 20+VAR_Y);
QPoint LBL_CODE(10+VAR_X, 60+VAR_Y);
QPoint LBL_NAME(10+VAR_X, 90+VAR_Y);
QPoint LBL_CRED(10+VAR_X, 120+VAR_Y);
QPoint LBL_H_TEO(250+VAR_X, 60+VAR_Y);
QPoint LBL_H_PRA(250+VAR_X, 90+VAR_Y);
QPoint LBL_H_LAB(250+VAR_X, 120+VAR_Y);

QPoint TXB_CODE_FIND(10+VAR_X,170+VAR_Y);
QPoint TXB_CODE(100+VAR_X, 60+VAR_Y);
QPoint TXB_NAME(100+VAR_X, 90+VAR_Y);
QPoint TXB_CRED(100+VAR_X, 120+VAR_Y);
QPoint TXB_H_TEO(320+VAR_X, 60+VAR_Y);
QPoint TXB_H_PRA(320+VAR_X, 90+VAR_Y);
QPoint TXB_H_LAB(320+VAR_X, 120+VAR_Y);

QSize BTN_SIZE_GENERAL(100, 40);
QSize BTN_SIZE_MOVES(40, 30);
QPoint BTN_ADD(470+VAR_X, 58+VAR_Y);
QPoint BTN_FIND(150+VAR_X,160+VAR_Y);
QPoint BTN_CLEAN(470+VAR_X, 103+VAR_Y);
QPoint BTN_START(205+VAR_X, 515+VAR_Y);
QPoint BTN_PREV(250+VAR_X, 515+VAR_Y);
QPoint BTN_NEXT(295+VAR_X, 515+VAR_Y);
QPoint BTN_END(340+VAR_X, 515+VAR_Y);

QPoint CBX_SEMESTER(420+VAR_X, 170+VAR_Y);
QSize CBX_SEMESTER_SIZE(150, 20);

QSize GRD_COURSE(560, 290);
QPoint GRD_COURSE_POS(10+VAR_X, 210+VAR_Y);

/*variables*/
/*typedef char ch;
typedef QString dataText;
typedef int PosE;
typedef unsigned int uPosE;
typedef double conv;
typedef std::string Str;
enum typeBlock {EXEC = 0, MOTI = 1, COND = 2, BUCL = 3, END = 4};*/

#endif // NTYPES
