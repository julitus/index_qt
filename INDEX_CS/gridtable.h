#ifndef GRIDTABLE_H
#define GRIDTABLE_H

#include <QTableWidget>

class GridTable: public QTableWidget{

    public:
        GridTable(QPoint point, QSize size, QWidget * parent = 0);
        ~GridTable();
};

#endif // GRIDTABLE_H
